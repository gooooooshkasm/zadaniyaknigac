﻿#include <iostream>
#include <math.h>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    setlocale(LC_ALL, "RUSSIAN");

    //1.1Вывести на одной строке числа 31, 18 и 79 с одним пробелом между ними.Текст '31 18 79' не использовать.
   
    cout << 31 << " " << 18 << " " << 79 << "\n"; 

    //1.2Вывести на одной строке числа 47, 52 и 150 с двумя пробелами между ними.Текст '47 52 150' не использовать.
    
    cout << 47 << "  " << 52 << "  " << 150 << "\n"; 

    //1.3Вывести на экран числа 50 и 10 одно под другим.
    
    cout << 50 << "\n" << 10 << "\n"; 

    //1.4.Вывести на экран числа 5, 10 и 21 одно под другим
    
    cout << 5 << "\n" << 10 << "\n" << 21 << "\n"; 

    /*1.5.Получить на экране следующее :
 
    1
 
    2
    */
    
    cout << "\n" << 1 << "\n\n" << 2 << "\n\n"; 

    //1.6 Число π примерно равно 3,1415926. Вывести на экран это число с тремя цифрами в дробной части.Текст '3.142' не использовать.
    
    double pi = 3.1415926;
    cout << "pi = " << setprecision(4) << pi << "\n\n"; 

    /*1.7 Число e (основание натурального логарифма)
    приблизительно равно 2, 71828. Вывести на экран это число с точностью до десятых.Текст '2.7' не использовать.*/
    
    double e = 2.71828;
    cout << "e = " << setprecision(2) << e << "\n\n"; 

    /*1.8 Составить программу вывода на экран числа, вводимого с клавиатуры.
    Выводимому числу должно предшествовать сообщение «Вы ввели число».*/
    
    cout << "Вы ввели число\n\n"; 

    /*1.9 Составить программу вывода на экран числа, вводимого с клавиатуры.
    После выводимого числа должно следовать сообщение «– вот какое число Вы ввели».*/
 
    int chislo19;
    
    cout << "Введите Ваше число\n";
    cin >> chislo19; 

    cout << chislo19 << " - вот какое число Вы ввели\n\n";

    /*1.10 Составить программу, которая запрашивает имя человека и повторяет его на экране.*/
 
    string imya;
    
    cout << "Введите Ваше имя  "; 
    cin >> imya;

    cout << "Ваше имя " << imya << "\n\n";

    /*1.11 Составить программу, которая запрашивает название футбольной команды и повторяет его на экране со словами «–это чемпион!».*/
  
    string komanda;
  
    cout << "Введите название футбольной команды "; 
    cin >> komanda;

    cout << komanda << " - это чемпион!" << "\n\n";

    /*1.12 Напишите программу, в которую вводится имя человека и выводится на экран приветствие
    в виде слова «Привет», после которого должна стоять запятая, введенное имя и восклицательный знак.
    После запятой должен стоять пробел, а перед восклицательным знаком пробела быть не должно.*/
   
    string imya1;
  
    cout << "Введите имя человека "; 
    cin >> imya1;

    cout <<"Привет, " << imya1 << "!" << "\n\n";

    /*1.13 Напишите программу, в которую вводится целое число, после чего на экран выводится следующее
    и предыдущее целое число.Например, при вводе числа 15 на экран должно быть выведено:
    Следующее за числом 15 число – 16.
    Для числа 15 предыдущее число – 14.*/
    
    int CeloeChislo;
  
    cout << "Введите целое число\n";
    cin >> CeloeChislo;

    cout << CeloeChislo - 1 << "\n" << CeloeChislo + 1<< "\n\n";

    /*1.14 Составить программу вывода на экран в одну строку трех любых чисел, вводимых с клавиатуры, с двумя пробелами между ними.*/
  
    double odin, dva, tri;
  
    cout << "Введите три любых числа\n";
    cout << "Первое число = \n";
    cin >> odin;

    cout << "Второе число = \n";
    cin >> dva;

    cout << "Третье число = \n";
    cin >> tri;

    cout << odin << "  "<< dva<< "  "<< tri << "\n\n";

    /*1.15 Составить программу вывода на экран в одну строку четырех любых чисел, вводимых с клавиатуры, с одним пробелом между ними.*/
    
    double odin1, dva1, tri1, chetire1; 
 
    cout << "Введите четыре любых числа\n";
    cout << "Первое число = \n";
    cin >> odin1;

    cout << "Второе число = \n";
    cin >> dva1;

    cout << "Третье число = \n";
    cin >> tri1;

    cout << "Четвертое число = \n";
    cin >> chetire1;

    cout << odin1 << " " << dva1 << " " << tri1 <<" " << chetire1 << "\n\n";

    /*
    1.16. Составить программу вывода на экран следующей ин-
    формации:
    а) 5 10 б)  100 t   в) x 25
       7 см    1949 v      x y
    Примечание
    t, v, x и y – переменные величины целого типа, значения
    которых вводятся с клавиатуры и должны быть выведены
    вместо имен величин.*/
    
    int odin2, dva2, tri2, chetire2;
  
    cout << "Введите четыре целочисленных числа\n";
    cout << "Первое число = \n";
    cin >> odin2;

    cout << "Второе число = \n";
    cin >> dva2;

    cout << "Третье число = \n";
    cin >> tri2;

    cout << "Четвертое число = \n";
    cin >> chetire2;

    cout << "a) " << 5 << " " << 10 << " " << "б) " << 100 << " " << odin2 << " в) " << dva2 << " " << 25 << "\n";
    cout << "   " << 7 << " " << "см" << "   " << 1949 << " " << tri2 << "    " << dva2 << " " << chetire2 << "\n\n";

    /*1.17.Составить программу вывода на экран следующей ин -
        формации:
        а) 2  кг б) а  1   в) x y
        13 17    19 b      5 y
        Примечание
        a, b, x и y – переменные величины целого типа, значения
        которых вводятся с клавиатуры и должны быть выведены
        вместо имен величин.
    */
    int odin3, dva3, tri3, chetire3;
 
    cout << "Введите четыре целочисленных числа\n";
    cout << "Первое число = \n";
    cin >> odin3;

    cout << "Второе число = \n";
    cin >> dva3;

    cout << "Третье число = \n";
    cin >> tri3;

    cout << "Четвертое число = \n";
    cin >> chetire3;

    cout << "a) " << 2 << "  " << "кг" << " " << "б) " << odin3 << " " << 1 << " в) " << dva3 << " " << tri3 << "\n";
    cout << "   " << 13 << " " << 17 << "   " << 19 << " " << chetire3 << "    " << 5 << " " << tri3 << "\n\n";
    



    /*
    2.1. Составить программу:
    а) вычисления значения функции y = 17x^2 – 6x + 13 при любом
    значении x;
    б) вычисления значения функции y = 3a^2 + 5a – 21 при любом
    значении а.
    */
    double x1 = 0.0, y1 = 0.0000000000, y2 = 0.000000000, a1 = 0.000000;
 
    cout << "Введите значение x = ";
    cin >> x1;

    y1 = 17 * (x1 * x1) - (6 * x1) + 13;

    cout << setprecision(15) << "При x = " << x1 << "   y = " << y1 << "\n\n";
    
    cout << "Введите значение a = ";
    cin >> a1;

    y2= 3 * (a1 * a1) + (5 * a1) - 21;
    cout << setprecision(15) << "При a = " << a1 << "   y = " << y2 << "\n\n";
   

    /*
    2.2. Составить программу вычисления значения функции
    при любом значении а.  (a^2 +10)/(sqrt(a^2+1))*/
  
    double y3, a2;
    
    cout << "Введи любое значение а = ";
    cin >> a2;

    y3 = (a2 * a2 + 10) / (sqrt(a2 * a2 + 1));
    cout << setprecision(15) << "При a = " << a2 << "   y = " << y3 << "\n\n";
 

    /*
    Составить программу:
    а) вычисления значения функции при любом sqrt((2*a+sin|3*a|)/3.56)
    значении а;
    б) вычисления значения функции при любом sin((3.2++sqrt(1+x))/|5x|)
    значении х.*/
  
    double a3, y4, x2, y5;

    cout << "Введи любое значение а = ";
    cin >> a3;

    y4 = sqrt((a3 * a3 + sin(abs(3 * a3))) / 3.56);
    cout << setprecision(15) << "При a = " << a3 << "   y = " << y4 << "\n\n";
    
    cout << "Введи любое значение x = ";
    cin >> x2;
    
    y5 = sin((3.2 + sqrt(1 + x2)) / (abs(5 * x2)));
    cout << setprecision(15) << "При x = " << x2 << "   y = " << y5 << "\n\n";
  
    /*
    2.4 Дана сторона квадрата. Найти его периметр*/
 
    double storonaKvadrata, perimetr;
    
    cout << "Введите сторону квадрата = ";
    cin >> storonaKvadrata;
    
    perimetr = storonaKvadrata * 4;
    cout << setprecision(15) << "Периметр квадрата = " <<perimetr << "\n\n";

    /*
    2.5. Дан радиус окружности. Найти ее диаметр.*/
 
    double radius, diametr;
   
    cout << "Введите радиус окружности = ";
    cin >> radius;
    
    diametr = 2 * radius;
    cout << setprecision(15) << "Диаметр окружности равен = " << diametr;

    /*
    2.6. Считая, что Земля – идеальная сфера с радиусом R ≈
    6350 км, определить расстояние до линии горизонта от точки
    с заданной высотой над Землей.*/

    double RadiusZemli = 6350, tochkaVisoti, rasstoyanieDoTochki;
    
    cout << "\n\nВведите высоту над Землей = ";
    cin >> tochkaVisoti;
   
    rasstoyanieDoTochki = sqrt(((RadiusZemli + tochkaVisoti) * (RadiusZemli + tochkaVisoti)) - pow(RadiusZemli, 2));
    cout << setprecision(15) << "Расстояние до линии горизонта от точки с заданной высотой над Землей = " << rasstoyanieDoTochki << "\n\n";

    /*
    2.7 Дана длина ребра куба. Найти объем куба и площадь его
    боковой поверхности.*/
  
    double dlinaRebra, obemKuba, ploschadKuba;
   
    cout << "Введите длину ребра куба = ";
    cin >> dlinaRebra;
   
    obemKuba = pow(dlinaRebra, 3);
    ploschadKuba = 6 * pow(dlinaRebra, 2);
    cout << setprecision(15) << "Объем куба равен V = " << obemKuba << "\n" << "Площадь куба S = " << ploschadKuba << "\n\n";

    /*
    2.8. Дан радиус окружности. Найти длину окружности и пло-
    щадь круга*/
   
    double radiusKruga = 0.0, p = 3.142, dlinaKruga, ploschadKruga = 0.0;
   
    cout << "Радиус круга равен R = ";
    cin >> radiusKruga;
   
    dlinaKruga = 2 * radiusKruga * p;
    ploschadKruga = p * pow(radiusKruga, 2);
    cout << setprecision (15) << "Длина круга L = " << dlinaKruga << "\n" << "Площадь круга S = " << ploschadKruga << "\n\n";


    /*
    2.9. Составить программу:
    а) вычисления значения функции z = 2x^3 – 3,44xy + 2,3x^2 – 7,1y
    + 2 при любых значениях х и y;
    б) вычисления значения функции x = 3,14(a + b)^3 + 2,75b^2 –
    12,7a – 4,1 при любых значениях a и b.
    */
   
    double znachenieZ, znachenieX, znachenieY, znachenieA, znachenieB, znachenieC;

    cout << "Введите значение Х = ";
    cin >> znachenieX;

    cout << "Введите значение Y = ";
    cin >> znachenieY;

    znachenieZ = 2 * pow(znachenieX, 3) - (3.44 * znachenieX * znachenieY) + 2.3 * pow(znachenieX, 2) - 7.1 * znachenieY + 2;
    cout << setprecision(15) << "Z = " << znachenieZ << "\n\n";

    cout << "Введите значение A = ";
    cin >> znachenieA;

    cout << "Введите значение B = ";
    cin >> znachenieB;

    znachenieC = 3.14 * pow((znachenieA + znachenieB), 3) + 2.75 * pow(znachenieB, 2) - 12.7 * znachenieA - 4.1;
    cout << setprecision(15) << "C = " << znachenieC << "\n\n"; 


        /*
        2.10. Даны два целых числа. Найти:
        а) их среднее арифметическое;
        б) их среднее геометрическое.
        */

    double Celoe111, Celoe222, Celoe333; 
    
    double arefm, geometr;

    cout << "Введите первое число: ";
    cin >> Celoe111;
    

    cout << "\nВведите второе число: ";
    cin >> Celoe222;
   

    arefm = (Celoe111 + Celoe222) / 2;
   
    Celoe333 = Celoe111 * Celoe222;
    geometr = pow (Celoe333, (1/2));

    cout << setprecision(15)<< "При целых числах: а = " << Celoe111 << " b = " << Celoe222 << "\n\nСреднее арефметическое равно = "
    << arefm << "\n\nА среднее геометрическое равно = " << geometr<<"\n\n";

    /*
    2.11. Известны объем и масса тела. Определить плотность ма-
    териала этого тела
    */
  
    double Obem, Massa, Plotnost;
    
    cout << "Введите объем тела V = ";
    cin >> Obem;

    cout << "\nВведите массу тела m = ";
    cin >> Massa;

    Plotnost = Obem / Massa;

    cout << setprecision(15) << "\nПри объеме тела V = " << Obem << "  и массе тела m = " << Massa << "\nПлотность тела p = " << Plotnost << "\n\n";

    /*
    2.12. Известны количество жителей в государстве и площадь
    его территории. Определить плотность населения в этом госу-
    дарстве.
    */
   
    double kolichestvoJitelei, PLoschadGosudarstva, PlotnostNaseleniya;
    
    cout << "Введите площадь государства = ";
    cin >> PLoschadGosudarstva;

    cout << "\nВведите количество жителей = ";
    cin >> kolichestvoJitelei;

    PlotnostNaseleniya = kolichestvoJitelei / PLoschadGosudarstva;

    cout << setprecision(15)<< "\nПри площади государства " << PLoschadGosudarstva << "  и количестве жителей государства " << kolichestvoJitelei
    << "\nПлотность плотность населения данного государтсва " << PlotnostNaseleniya << "\n\n";
    

    /*2.13.Составить программу решения линейного уравнения
    ax + b = 0 (a ≠ 0).*/

    
    double PervoeA, Iks, VtoroeB;

    cout << "Введите значение b" << endl << "b = ";
    cin >> VtoroeB;
    cout << "Введите значение а" << endl << "a = ";
    cin >> PervoeA;

    if (PervoeA == 0)
    {
        cout << "Значение \"a\" не должно равнять 0, введите другое значение а" << endl;
        cout << "a = ";
        cin >> PervoeA;
    }

    cout << "Решение линейного уравнения ax + b = 0, при значениях a = " << PervoeA << " и значениях b = " << VtoroeB << endl;
    Iks = -(VtoroeB / PervoeA);

    cout << setprecision(15) << "\n\n x = " << Iks << "\n\n";
    

    /*2.14. Даны катеты прямоугольного треугольника. Найти его гипотенузу.*/

    
    double Katet1, Katet2, Gipotinuza;

    cout << "Введите значение первого катета, a = ";
    cin >> Katet1;
    cout << "Введите значение второго катета, b = ";
    cin >> Katet2;

    if (Katet1 <=0 )
    {
        cout << "Введите значение первого катета больше нуля, a = ";
        cin >> Katet1;
    }

    if (Katet2 <= 0)
    {
        cout << "Введите значение второго катета больше нуля, b = ";
        cin >> Katet2;
    }

    Gipotinuza = sqrt(pow(Katet1, 2) + pow(Katet2, 2));
    cout << setprecision(15) << "Гипотенуза будет, c = " << Gipotinuza << endl << endl;
    

    /*2.15.Найти площадь кольца по заданным внешнему и внутреннему
    радиусам.*/

    double VneshniiR, VnutrenniiR, PloschadKolca;

    cout << "Введите значение внешнего радиуса кольца, R = ";
    cin >> VneshniiR;
    cout << "Введите значение внутреннего радиуса кольца, r = ";
    cin >> VnutrenniiR;
    
    if (VneshniiR < VnutrenniiR || VneshniiR == 0 || VnutrenniiR == 0 || VneshniiR == VnutrenniiR)
    {
        cout << 
        "Введите другие значение внешнего и внутреннего радиуса кольца, чтобы радиус внешнего кольца был больше радиус внутреннего кольца" 
        << endl; 
       
        cout << "Введите значение внешнего радиуса кольца, R = ";
        cin >> VneshniiR;
        cout << "Введите значение внутреннего радиуса кольца, r = ";
        cin >> VnutrenniiR;
    }

    PloschadKolca = 3.1415926 * (pow(VneshniiR, 2) - pow(VnutrenniiR, 2));
    cout << "Площадь кольца будет, S = " << setprecision(15) << PloschadKolca << endl << endl;
    

    /*
    2.16. Даны катеты прямоугольного треугольника. Найти его периметр.
    */
    double Katet3, Katet4, Gipotinuza1, Perimetr12;

    cout << "Введите значение первого катета, a = ";
    cin >> Katet3;
    cout << "Введите значение второго катета, b = ";
    cin >> Katet4;

    if (Katet3 <= 0)
    {
        cout << "Введите значение первого катета больше нуля, a = ";
        cin >> Katet3;
    }

    if (Katet4 <= 0)
    {
        cout << "Введите значение второго катета больше нуля, b = ";
        cin >> Katet4;
    }

    Gipotinuza1 = sqrt(pow(Katet3, 2) + pow(Katet4, 2));
    Perimetr12 = Gipotinuza1 + Katet3 + Katet4;
    cout << setprecision(15) << "Периметр равен, P = " << Perimetr12 << endl << endl;
    

    /*
    2.17. Даны основания и высота равнобедренной трапеции. Найти ее периметр.
    */
 
    double Osnovanie1, Osnovanie2, VisotaTrop, PerimetrTrop, UchastokTrop;

    cout << "Введите высоту тропеции, H = ";
    cin >> VisotaTrop;
    cout << "Введите верхнюю тропецию, C = ";
    cin >> Osnovanie1;
    cout << "Введите нижнюю тропецию, D = ";
    cin >> Osnovanie2;

    if (VisotaTrop <= 0 )
    {
        cout << "Введите высоту тропеции больше 0, H = ";
        cin >> VisotaTrop;
    }

    if (Osnovanie1 <= 0) 
    {
        cout << "Введите верхнюю тропецию больше 0, C = ";
        cin >> Osnovanie1;
    }

    if (Osnovanie2 <= 0) 
    {
        cout << "Введите нижнюю тропецию больше 0, D = ";
        cin >> Osnovanie2;
    }
    
    UchastokTrop = (Osnovanie2 - Osnovanie1) / 2;
    PerimetrTrop = Osnovanie1 + Osnovanie2 + 2 * (sqrt(pow(UchastokTrop, 2) + pow(VisotaTrop, 2)));
   
    //по сути равнобедренная тропеция с равными основаниями это прямоугольник и высоту в таком случае нельзя провести, если только
    //не считать, что высота это сторона прямоугольника, тогда решение очень простое сумма основания (любого) с высотой и все это 
    // умноженное на 2 (а+b)*2, но что то такое было сверху

    if (Osnovanie1 > Osnovanie2)  
    {
        UchastokTrop = (Osnovanie1 - Osnovanie2) / 2;
        PerimetrTrop = Osnovanie1 + Osnovanie2 + 2 * (sqrt(pow(UchastokTrop, 2) + pow(VisotaTrop, 2)));
    }
    
    cout << setprecision(15) << "Периметр равнобедренной трапеции равен, P = " << PerimetrTrop << endl<< endl;
    

    /*2.18. Составить программу вычисления значений функций при любых значениях х и y.
    */  

    double ZnachenieXX, ZnachenieYY, ZnachenieZZ, ZnachenieQQ;

    cout << "Введите значение х, х = ";
    cin >> ZnachenieXX;
    cout << "Введите значение y, y = ";
    cin >> ZnachenieYY;

    //Задание странно сформулировано, чтобы искало значение при любых x и y, но при x = 0, но там в примере 
    // выражение (2+y) / x^2, что напрашивается на условие, что х != 0
    
    ZnachenieZZ = (ZnachenieXX + ((2 + ZnachenieYY) / pow(ZnachenieXX, 2))) / (ZnachenieYY + (1 / (sqrt(pow(ZnachenieXX, 2) + 10))));
    cout << setprecision(15) << "z = " << ZnachenieZZ << endl;

    ZnachenieQQ = 7.25 * sin(ZnachenieXX) - abs(ZnachenieYY);
    cout << setprecision(15) << "q = " << ZnachenieQQ << endl << endl;
    

    /*2.19. Составить программу расчета значения функций при любых значениях a и b.
    */
   
    double ZnachenieXXX, ZnachenieAA, ZnachenieBB, ZnachenieYYY;

    cout << "Введите значение a, a = ";
    cin >> ZnachenieAA;
    cout << "Введите значение b, b = ";
    cin >> ZnachenieBB;

    ZnachenieXXX = (2 / (pow(ZnachenieAA, 2) + 25) + ZnachenieBB) / (sqrt(ZnachenieBB) + (ZnachenieAA + ZnachenieBB) / 2);
    cout << setprecision(15) << "x = " << ZnachenieXXX << endl;

    ZnachenieYYY = (abs(ZnachenieAA) + 2 * sin(ZnachenieBB)) / (5.5 * ZnachenieAA);
    cout << setprecision(15) << "y = " << ZnachenieYYY << endl << endl;
    

    /*
    2.20. Составить программу расчета значения функций
    при любых значениях e, f, g и h.
    */
    
    double ZnachenieE, ZnachenieF, ZnachenieG, ZnachenieH, ZnacheieAAA, ZnachenieBBB, ZnachenieCC;

    cout << "Введите значение e, e = ";
    cin >> ZnachenieE;
    cout << "Введите значение f, f = ";
    cin >> ZnachenieF;
    cout << "Введите значение g, g = ";
    cin >> ZnachenieG;
    cout << "Введите значение h, h = ";
    cin >> ZnachenieH;

    ZnacheieAAA = sqrt(pow(abs(ZnachenieE - 3 / ZnachenieF), 3) + ZnachenieG);
    cout << setprecision(15) << "a = " << ZnacheieAAA << endl << endl;

    ZnachenieBBB = sin(ZnachenieE) + pow(cos(ZnachenieH), 2);
    cout << setprecision(15) << "b = " << ZnachenieBBB << endl << endl;

    ZnachenieCC = (33 * ZnachenieG) / (ZnachenieE * ZnachenieF - 3);
    cout << setprecision(15) << "c = " << ZnachenieCC << endl << endl;
    

    /*2.21. Составить программу расчета значения функций при любых значениях e, f, g и h.
    */
    
    double ZnachenieEE, ZnachenieFF, ZnachenieGG, ZnachenieHH, ZnachenieAAAA, ZnachenieBBBB, ZnachenieCCC;

    cout << "Введите значение e, e = ";
    cin >> ZnachenieEE;
    cout << "Введите значение f, f = ";
    cin >> ZnachenieFF;
    cout << "Введите значение g, g = ";
    cin >> ZnachenieGG;
    cout << "Введите значение h, h = ";
    cin >> ZnachenieHH;

    ZnachenieAAAA = (ZnachenieEE + ZnachenieFF / 2) / 3;
    cout << setprecision(15) << "a = " << ZnachenieAAAA << endl << endl;
    
    ZnachenieBBBB = abs(pow(ZnachenieHH, 2) - ZnachenieGG);
    cout << setprecision(15) << "b = " << ZnachenieBBBB << endl << endl;

    ZnachenieCCC = sqrt(pow((ZnachenieGG - ZnachenieHH), 2) - 3 * sin(ZnachenieEE));
    cout << setprecision(15) << "c = " << ZnachenieCCC << endl << endl;
    

    /*2.22. Даны два числа. Найти среднее арифметическое и среднее геометрическое их модулей
    */
    
    double ChisloAA, ChisloBB, ArifmAB, GeometrAB;
    
    cout << "Введите первое число, А = ";
    cin >> ChisloAA;
    cout << "Введите второе число, B = ";
    cin >> ChisloBB;

    ArifmAB = abs(ChisloAA + ChisloBB) / 2;
    cout << setprecision(15) << "Среднее арифметическое значение двух чисел по их модулю - " << ArifmAB << endl << endl;

    GeometrAB = sqrt(abs(ChisloAA * ChisloBB));
    cout << setprecision(15) << "Среднее геометрическое значение двух чисел по их модулю - " << GeometrAB << endl << endl;
    

    /*
    2.23. Даны стороны прямоугольника. Найти его периметр и длину диагонали.
    */

    double StoronaPryamA, StoronaPryamB, DiagPryam, PerPryam;

    cout << "Введите первую сторону прямоугольника, А = ";
    cin >> StoronaPryamA;
    cout << "Введите вторую сторону прямоугольника, B = ";
    cin >> StoronaPryamB;

    if (StoronaPryamA <= 0)
    {
        cout << "Введите первую сторону прямоугольника больше 0, А = ";
        cin >> StoronaPryamA;
    }
    if (StoronaPryamB <= 0)
    {
        cout << "Введите вторую сторону прямоугольника больше 0, В = ";
        cin >> StoronaPryamB;
    }
    
    PerPryam = 2 * (StoronaPryamA + StoronaPryamB);
    cout << setprecision(15) << "Периметр прямоугольника равен, Р = " << PerPryam << endl;

    DiagPryam = sqrt(pow(StoronaPryamA, 2) + pow(StoronaPryamB, 2));
    cout << setprecision(15) << "Диагональ прямоугольника равен, D = " << DiagPryam << endl << endl;
    

    /*
    2.24. Даны два числа. Найти их сумму, разность, произведение,
    а также частное от деления первого числа на второе.
    */

    double ChisloT, ChisloP, Sum1, Raz, Proiz, Delen;

    cout << "Введите первое число, А = ";
    cin >> ChisloT;
    cout << "Введите второе число, B = ";
    cin >> ChisloP;

    if (ChisloP == 0)
    {
        cout << "Введите второе число не равное 0, B = ";
        cin >> ChisloP;
    }

    Sum1 = ChisloT + ChisloP;
    cout << setprecision(15) << "Сумма чисел равна, Sum = " << Sum1 << endl << endl;

    Raz = ChisloT - ChisloP;
    cout << setprecision(15) << "Разность чисел равна, Raznost = " << Raz << endl << endl;

    Proiz = ChisloT * ChisloP;
    cout << setprecision(15) << "Произведение чисел равна, Proizvedenie = " << Proiz << endl << endl;

    Delen = ChisloT / ChisloP;
    cout << setprecision(15) << "Деление чисел равна, Delenie = " << Delen << endl << endl;
    

    /*2.25. Даны длины сторон прямоугольного параллелепипеда.
    Найти его объем и площадь боковой поверхности.
    */

    double StoronaParalA, StoronaParalB, StoronaParalH, VParal, SBokovoiParal;

    cout << "Введите первую сторону прямоугольного параллепипеда, А = ";
    cin >> StoronaParalA;
    cout << "Введите первую сторону прямоугольного параллепипеда, B = ";
    cin >> StoronaParalB;
    cout << "Введите первую сторону прямоугольного параллепипеда, H = ";
    cin >> StoronaParalH;

    if (StoronaParalA <= 0)
    {
        cout << "Введите первую сторону прямоугольного параллепипеда больше 0, А = ";
        cin >> StoronaParalA;
    }

    if (StoronaParalB <= 0)
    {
        cout << "Введите вторую сторону прямоугольного параллепипеда больше 0, B = ";
        cin >> StoronaParalB;
    }

    if (StoronaParalH <= 0)
    {
        cout << "Введите третью сторону прямоугольного параллепипеда больше 0, H = ";
        cin >> StoronaParalH;
    }

    VParal = StoronaParalA * StoronaParalB * StoronaParalH;
    SBokovoiParal = 2 * (StoronaParalA * StoronaParalH + StoronaParalB * StoronaParalH);

    cout << endl << setprecision(15) << "Объем прямоугольного параллепипеда равен, V = " << VParal << endl << endl;
    cout << setprecision(15) << "Площадь боковой поверхности равен, Sбок = " << SBokovoiParal << endl << endl;
    
    
    /*
    2.26. Даны координаты на плоскости двух точек. Найти расстояние между этими точками.
    */
    
    double TochkaX1, TochkaX2, TochkaY1, TochkaY2, Obchee;

    cout << "Введите координаты первой точки \n\n" << "x1 = ";
    cin >> TochkaX1;
    cout << "Введите координаты первой точки \n\n" << "y1 = ";
    cin >> TochkaY1;

    cout << "Введите координаты второй точки \n\n" << "x2 = ";
    cin >> TochkaX2;
    cout << "Введите координаты второй точки \n\n" << "y2 = ";
    cin >> TochkaY2;

    Obchee = sqrt(pow((TochkaX2 - TochkaX1), 2) - pow((TochkaY2 - TochkaY1), 2));
    cout << setprecision(15) << "Расстояние между данными точками равно, AB = " << Obchee << endl << endl;
    

    /*
    2.27. Даны основания и высота равнобедренной трапеции.Найти периметр трапеции.

    Повторяющее задание!!!
    */

    /*
    2.28. Даны основания равнобедренной трапеции и угол при
    большем основании. Найти площадь трапеции.
    */
    
    double OsnovanieTrapVerh, OsnovanieTrapNiz, UgolA, RadiusTrap, STrap;

    cout << "Верхнее основание равнобедренной трапеции \n\n" << "A = ";
    cin >> OsnovanieTrapVerh;
    cout << "\nВведите нижнее основание трапеции \n" << "B = ";
    cin >> OsnovanieTrapNiz;
    cout << "\nВведите угол при большом основании трапеции \n" << "a = ";
    cin >> UgolA;

    if (UgolA < 0 || UgolA> 90)
    {
        cout << "Введите значение угла удовлетворяющим условиям от 0 до 90"<<endl<< "a = ";
        cin >> UgolA;
    }

    if (OsnovanieTrapVerh <= 0)
    {
        cout << "Введите верхнюю тропецию больше 0, A = ";
        cin >> OsnovanieTrapVerh;
    }

    if (OsnovanieTrapNiz <= 0)
    {
        cout << "Введите нижнюю тропецию больше 0, B = ";
        cin >> OsnovanieTrapNiz;
    }
    
    RadiusTrap = ((OsnovanieTrapNiz - OsnovanieTrapVerh) / 2) / (2 * tan(UgolA)); 

    STrap = abs((4 * pow(RadiusTrap, 2)) / sin(UgolA));

    cout << setprecision(15) << "Площадь трапеции равна, Sтрап = " << STrap << endl <<endl;
    

    /*
    2.29. Треугольник задан координатами своих вершин. Найти периметр и площадь треугольника.
    */
    
    double TochkaTreugX1, TochkaTreugX2, TochkaTreugX3, TochkaTreugY1, TochkaTreugY2, TochkaTreugY3, PerimTreug, STreug;
   
    cout << "Введите координаты первой вершины " << endl << "x1 = ";
    cin >> TochkaTreugX1;
    cout << "y1 = ";
    cin >> TochkaTreugY1;

    cout << "Введите координаты второй вершины " << endl << "x2 = ";
    cin >> TochkaTreugX2;
    cout << "y2 = ";
    cin >> TochkaTreugY2;

    cout << "Введите координаты третьей вершины " << endl << "x3 = ";
    cin >> TochkaTreugX3;
    cout << "y3 = ";
    cin >> TochkaTreugY3;

    PerimTreug = sqrt(pow((TochkaTreugX2 - TochkaTreugX1), 2) + pow((TochkaTreugY2 - TochkaTreugY1), 2)) +
        sqrt(pow((TochkaTreugX3 - TochkaTreugX1), 2) + pow((TochkaTreugY3 - TochkaTreugY1), 2)) +
        sqrt(pow((TochkaTreugX3 - TochkaTreugX2), 2) + pow((TochkaTreugY3 - TochkaTreugY2), 2));

    // Уже когда сделал, подумал, что можно было для удобства стороны обозначить переменными для удобства 
    //но когда все написал, было лень переделывать вроде в скобочках не ошибся 

    STreug = sqrt((PerimTreug / 2) *
        ((PerimTreug / 2) - (sqrt(pow((TochkaTreugX2 - TochkaTreugX1), 2) + pow((TochkaTreugY2 - TochkaTreugY1), 2)))) *
        ((PerimTreug / 2) - (sqrt(pow((TochkaTreugX3 - TochkaTreugX1), 2) + pow((TochkaTreugY3 - TochkaTreugY1), 2)))) *
        ((PerimTreug / 2) - (sqrt(pow((TochkaTreugX3 - TochkaTreugX2), 2) + pow((TochkaTreugY3 - TochkaTreugY2), 2)))));

    cout << setprecision(15) << "Периметр треугольника равен, Pтреуг = " << PerimTreug 
        << endl << "Площадь равна, Sтреуг = " << STreug << endl << endl;


    /*
    2.30. Выпуклый четырехугольник задан координатами своих
    вершин. Найти площадь этого четырехугольника как сумму пло-
    щадей треугольников.
    */

    double TochkaChetX1, TochkaChetX2, TochkaChetX3, TochkaChetY1, TochkaChetY2, TochkaChetY3, TochkaChetX4, TochkaChetY4,
        StoronaAB, StoronaBC, StoronaAC, StoronaAD, StoronaDC, PerimTreug1, STreug1, PerimTreug2, STreug2, SChetireh;

    cout << "Введите координаты первой вершины " << endl << "x1 = ";
    cin >> TochkaChetX1;
    cout << "y1 = ";
    cin >> TochkaChetY1;

    cout << "Введите координаты второй вершины " << endl << "x2 = ";
    cin >> TochkaChetX2;
    cout << "y2 = ";
    cin >> TochkaChetY2;

    cout << "Введите координаты третьей вершины " << endl << "x3 = ";
    cin >> TochkaChetX3;
    cout << "y3 = ";
    cin >> TochkaChetY3;

    cout << "Введите координаты четвертой вершины " << endl << "x3 = ";
    cin >> TochkaChetX4;
    cout << "y3 = ";
    cin >> TochkaChetY4;

    StoronaAB = sqrt(pow((TochkaChetX2 - TochkaChetX1), 2) + pow((TochkaChetY2 - TochkaChetY1), 2));

    StoronaBC = sqrt(pow((TochkaChetX3 - TochkaChetX2), 2) + pow((TochkaChetY3 - TochkaChetY2), 2));

    StoronaAC = sqrt(pow((TochkaChetX3 - TochkaChetX1), 2) + pow((TochkaChetY3 - TochkaChetY1), 2));

    StoronaAD = sqrt(pow((TochkaChetX4 - TochkaChetX1), 2) + pow((TochkaChetY4 - TochkaChetY1), 2));

    StoronaDC = sqrt(pow((TochkaChetX4 - TochkaChetX3), 2) + pow((TochkaChetY4 - TochkaChetY3), 2));

    PerimTreug1 = StoronaAB + StoronaAC + StoronaBC;
    PerimTreug2 = StoronaAD + StoronaAC + StoronaDC;

    STreug1 = sqrt((PerimTreug1 / 2) * ((PerimTreug1 / 2) - StoronaAB) * ((PerimTreug1 / 2) - StoronaAC) * ((PerimTreug1 / 2) - StoronaBC));
    STreug2 = sqrt((PerimTreug2 / 2) * ((PerimTreug2 / 2) - StoronaAD) * ((PerimTreug2 / 2) - StoronaAC) * ((PerimTreug2 / 2) - StoronaDC));

    SChetireh = STreug1 + STreug2;

    cout << setprecision(15) << "Площадь выпуклого четырехугольника равен, S = " << SChetireh << endl << endl;


    /*
    2.31. Известна стоимость 1 кг конфет, печенья и яблок. Найти
    стоимость всей покупки, если купили x кг конфет, у кг печенья
    и z кг яблок.
    */

    double CenaKonfet, CenaPechenya, CenaYablok, VesKonfet, VesPechenya, VesYablok, SummaPokupok;

    cout << "Введите цену за 1 кг конфет, Цена конф = ";
    cin >> CenaKonfet;

    cout << "Введите цену за 1 кг печенья, Цена печ = ";
    cin >> CenaPechenya;

    cout << "Введите цену за 1 кг яблок, Цена яблок = ";
    cin >> CenaYablok;

    cout << "Введите сколько киллограмм конфет купили, m конф = ";
    cin >> VesKonfet;

    cout << "Введите сколько киллограмм печенья купили, m печ = ";
    cin >> VesPechenya;

    cout << "Введите сколько киллограмм яблок купили, m яблок = ";
    cin >> VesYablok;

    if (CenaKonfet < 0 || CenaPechenya < 0 || CenaYablok < 0 || VesKonfet < 0 || VesPechenya < 0 || VesYablok < 0)
    {
        cout << "Введите все значения больше 0" << endl;
        cout << "Введите цену за 1 кг конфет, Цена конф = ";
        cin >> CenaKonfet;

        cout << "Введите цену за 1 кг печенья, Цена печ = ";
        cin >> CenaPechenya;

        cout << "Введите цену за 1 кг яблок, Цена яблок = ";
        cin >> CenaYablok;

        cout << "Введите сколько киллограмм конфет купили, m конф = ";
        cin >> VesKonfet;

        cout << "Введите сколько киллограмм печенья купили, m печ = ";
        cin >> VesPechenya;

        cout << "Введите сколько киллограмм яблок купили, m яблок = ";
        cin >> VesYablok;
    }
   
    SummaPokupok = CenaKonfet * VesKonfet + CenaPechenya * VesPechenya + CenaYablok * VesYablok;
    cout << setprecision(15) << "Сумма всех покупок равна, Summa = " << SummaPokupok << endl << endl;
    

    /*
    2.32. Известна стоимость монитора, системного блока, клавиатуры
    и мыши. Сколько будут стоить 3 компьютера из этих эле-
    ментов? N компьютеров?
    */

    double CenaMonit, CenaBlock, CenaKlav, CenaMishi, KolichestvoKomp, Komp3, KompN;

    cout << "Введите цену за монитор, Цена мон = ";
    cin >> CenaMonit;

    cout << "\nВведите цену за системного блока, Цена блока = ";
    cin >> CenaBlock;

    cout << "\nВведите цену за клавиатуру, Цена клав = ";
    cin >> CenaKlav;

    cout << "\nВведите цену за мышь, Цена мышь = ";
    cin >> CenaMishi;

    Komp3 = 3 * (CenaBlock + CenaKlav + CenaMishi + CenaMonit);
    cout << setprecision (15) << "Стоимость 3 компьютеров будет равна, Цена 3 комп = " << Komp3 << endl;

    cout << "\nВведите количество компьютеров, которые необходимо купить = ";
    cin >> KolichestvoKomp;

    KompN = KolichestvoKomp * (CenaBlock + CenaKlav + CenaMishi + CenaMonit);
    cout<< setprecision(15) << "Стоимость "<< KolichestvoKomp<<" компьютеров будет равна, Цена комп = " << KompN << endl << endl;
    

    /*
    2.33. Возраст Тани – X лет, а возраст Мити – Y лет. Найти их
    средний возраст, а также определить, на сколько отличается воз-
    раст каждого ребенка от среднего значения.
    */

    double VozrastTani, VozrastMiti, SredniiVozrast, OtlichTani, OtlichMiti;

    cout << "Введите возраст Тани = ";
    cin >> VozrastTani;

    cout << "Введите возраст Мити = ";
    cin >> VozrastMiti;

    SredniiVozrast = (VozrastTani + VozrastMiti) / 2;
    cout << "Средний возраст Тани и Мити = " << SredniiVozrast << endl;

    OtlichTani = abs(SredniiVozrast - VozrastTani);
    OtlichMiti = abs(SredniiVozrast - VozrastMiti);

    cout << "Возраст Тани отличается от среднего возраста = " << OtlichTani << endl;
    cout << "Возраст Мити отличается от среднего возраста = " << OtlichMiti << endl << endl;
    

    /*
    2.34. Два автомобиля едут навстречу друг другу с постоянны-
    ми скоростями V1 и V2 км/ч. Определить, через какое время ав-
    томобили встретятся, если расстояние между ними было S км.
    */

    double Skorost1, Skorost2, Vremya, RasstoyanieS;

    cout << "Введите скорость первого водителя, V1 = ";
    cin >> Skorost1;

    cout << "Введите скорость второго водителя, V2 = ";
    cin >> Skorost2;

    cout << "Введите расстояние между водителями, S = ";
    cin >> RasstoyanieS;

    Vremya = RasstoyanieS / (Skorost1 + Skorost2);
    cout << setprecision(15) << "Через " << Vremya << " часов/минут/секунд встретятся водителя" << endl << endl;


    /*
    2.35. Два автомобиля едут друг за другом с постоянными ско-
    ростями V1 и V2 км/ч (V1 > V2). Определить, какое расстояние будет
    между ними через 30 мин после того, как первый автомобиль
    опередил второй на S км.
    */

    double SkorostMash1, SkorostMash2, Path1, Path2;

    cout << "Введите скорость первого водителя, V1 = ";
    cin >> SkorostMash1;

    cout << "Введите скорость второго водителя, V2 = ";
    cin >> SkorostMash2;

    cout << "Введите расстояние на сколько первый водитель опережает второго, S = ";
    cin >> Path1;

    if (SkorostMash1 < SkorostMash2)
    {
        cout << "Введите скорость водителей, так чтобы скорость первого было больше скорости второго (V1>V2)" << endl << endl;
        
        cout << "Введите скорость первого водителя, V1 = ";
        cin >> SkorostMash1;

        cout << "Введите скорость второго водителя, V2 = ";
        cin >> SkorostMash2;
    }

    Path2 = Path1 + (SkorostMash1 - SkorostMash2) * 0.5;

    cout << setprecision(10) << "Расстояние между водителями через 30 минут будет равно, S = " << Path2 << endl << endl;



    /*
    2.36. Известно значение температуры по шкале Цельсия. Най-
    ти соответствующее значение температуры по шкале:
    а) Фаренгейта;
    б) Кельвина.
    Для пересчета по шкале Фаренгейта необходимо исходное зна-
    чение температуры умножить на 1,8 и к результату прибавить
    32, а по шкале Кельвина абсолютное значение нуля соответствует
    –273,15 градуса по шкале Цельсия.
    */

    double TempCels, TempFar, TempKel;

    cout << "Введите температуру в Целььсиях, T = ";
    cin >> TempCels;

    if (TempCels < -273.15)
    {
        cout << "Такая темепратура не возможна, введите значение температуры больше, чем -273.15" << endl;
        cout << "Введите температуру в Целььсиях, T = ";
        cin >> TempCels;
    }

    TempFar = 1.8 * TempCels + 32;
    cout << setprecision(15) << "Температура в Фаренгейтах будет равна, Tf = " << TempFar << endl << endl;

    TempKel = TempCels + 273.15;
    cout << setprecision(15)<< "Температура в Кельвинах будет равна, Tk = " << TempKel << endl << endl;


    /*
     2.37. У американского писателя-фантаста Рэя Бредбери есть
    роман «450 градусов по Фаренгейту». Разработать программу,
    которая определяет, какой температуре по шкале Цельсия соот-
    ветствует указанное в названии значение. (См. предыдущую за-
    дачу.)
    */


    double TempCel1;

    TempCel1 = (450 - 32) / 1.8;

    cout << setprecision(10) << "450 градусов по фаренгейту будет соответсовать температуре по Цельсию, Tc = " << TempCel1 << endl << endl;


    /*
    2.38. Напишите программу, в которой вычисляется сумма, раз-
    ность, произведение, частное и среднее арифметическое двух це-
    лых чисел, введенных с клавиатуры. Например, при вводе чисел
    2 и 7 должен быть получен ответ вида:
    2+7=9 2-7=-5 2*7=14 2/7=0.2857142857142857 (2+7)/2=4.5
    */

    double ChastnoeChisel, SredneeArifmeticheskoeChisel;
    double  ChisloCeloe1, ChisloCeloe2, SummaChisel, RaznostChisel, ProizvedenieChisel;

    cout << "Введите первое целое число, а = ";
    cin >> ChisloCeloe1;

    cout << "Введите второе целое число, b = ";
    cin >> ChisloCeloe2;

    if (ChisloCeloe2 == 0)
    {
        cout << "Введите второе целое число больше 0, b = ";
        cin >> ChisloCeloe2;
    }

    SummaChisel = ChisloCeloe1 + ChisloCeloe2;
    cout << "\nСумма данных чисел равна, Sum = " << SummaChisel << endl << endl;

    RaznostChisel = ChisloCeloe1 - ChisloCeloe2;
    cout << "Разность данных чисел равна, Raz = " << RaznostChisel << endl << endl;

    ProizvedenieChisel = ChisloCeloe1 * ChisloCeloe2;
    cout << "Произведение данных чисел равна, Proizvedenie = " << ProizvedenieChisel << endl << endl;

    ChastnoeChisel = ChisloCeloe1 / ChisloCeloe2;
    cout << setprecision(10) << "Частное данных чисел равна, Chast = " << ChastnoeChisel << endl << endl;

    SredneeArifmeticheskoeChisel = SummaChisel / 2;
    cout << setprecision(10) << "Среднее арифметическое данных чисел равна, SredArifm = " << SredneeArifmeticheskoeChisel << endl << endl;
    }
